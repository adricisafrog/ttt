// TTT.cpp : Defines the entry point for the console application.
//

// TicTacToe.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
using namespace std;


class Board
{
public:
	string panel[3][3] = { "_","_","_","_","_","_","_","_","_" };
	string falsePanel[3][3] = { "_","_","_","_","_","_","_","_","_" };
	int turn = 0;
	int falseTurn = 1;
	int add1 = 1;
	int valueA[3][3][10];

	void Show(string board[3][3]);
	void PlayerTurn(string player);
	void setValues(int across, int down, string player, string board[3][3]);
	int Win(string player, string board[3][3]);
	int RecursiveWinCheck();
	int ComWinCheck();

};


class Turn
{
public:
	int turn = 0;
	void NextTurn();

};

void Turn::NextTurn()
{
	turn = turn + 1;
}


void Board::Show(string board[3][3])
{
	cout << endl << "  Row:" << endl;
	cout << "      +---+---+---+" << endl;
	cout << "   0  | " << board[0][0] << " | " << board[1][0] << " | " << board[2][0] << " |" << endl;
	cout << "      +---+---+---+" << endl;
	cout << "   1  | " << board[0][1] << " | " << board[1][1] << " | " << board[2][1] << " |" << endl;
	cout << "      +---+---+---+" << endl;
	cout << "   2  | " << board[0][2] << " | " << board[1][2] << " | " << board[2][2] << " |" << endl;
	cout << "      +---+---+---+" << endl;
	cout << "         Column:  " << endl;
	cout << "        0   1   2  " << endl;
}
void Board::setValues(int across, int down, string player, string board[3][3])
{
	board[across][down] = player;
}
int Board::Win(string player, string board[3][3])
{
	for (size_t i = 0; i < 3; i++)
	{

		if (board[i][0] == player && board[i][1] == player && board[i][2] == player)
		{
			if (board != falsePanel)
			{
				cout << player << " wins" << endl;
			}
			if (player == "X")
			{
				return 10;
			}
			else
			{
				return -10;
			}
		}
		if (board[0][i] == player && board[1][i] == player && board[2][i] == player)
		{
			if (board != falsePanel)
			{
				cout << player << " wins" << endl;
			}
			if (player == "X")
			{
				return 10;
			}
			else
			{
				return -10;
			}
		}
	}
	if (board[0][0] == player && board[1][1] == player && board[2][2] == player)
	{
		if (board != falsePanel)
		{
			cout << player << " wins" << endl;
		}
		if (player == "X")
		{
			return 10;
		}
		else
		{
			return -10;
		}
	}
	if (board[2][0] == player && board[1][1] == player && board[0][2] == player)
	{
		if (board != falsePanel)
		{
			cout << player << " wins" << endl;
		}
		if (player == "X")
		{
			return 10;
		}
		else
		{
			return -10;
		}
	}
	else if (board[0][0] != "_" && board[1][0] != "_" && board[2][0] != "_" && board[0][1] != "_" && board[1][1] != "_" && board[2][1] != "_" && board[0][2] != "_" && board[1][2] != "_" && board[2][2] != "_")
	{
		if (board != falsePanel)
		{
			cout << "Draw" << endl;
		}
		return 0;
	}
	return 2;
}
int Board::RecursiveWinCheck()
{

	string player;
	int valueWin;
	int valuef = 0;
	int valueX = 10;
	int valueO = -10;
	int place[2];

	if (turn % 2 == 0 && panel[1][1] == "_")
	{
		panel[1][1] = "O";
		falsePanel[1][1] = "O";
		return 0;
	}


	for (int a = 0; a < 3; a++)
	{
		for (int b = 0; b < 3; b++)
		{
			if (falsePanel[a][b] == "_")
			{
				if (falseTurn % 2 == 0)
				{
					player = "X";
				}
				else
				{
					player = "O";
				}

				falsePanel[a][b] = player;

				valueWin = Win(player, falsePanel);
				if (valueWin != 2)
				{
					falsePanel[a][b] = "_";
					return valueWin;
				}

				falseTurn++;
				add1++;
				valueA[a][b][add1 - 1] = RecursiveWinCheck();
				falseTurn++;
				add1--;
				falsePanel[a][b] = "_";
			}

		}

	}

	if (add1 == 1)
	{
		for (int c = 0; c < 3; c++)
		{
			for (int C = 0; C < 3; C++)
			{
				if (panel[c][C] == "_")
				{
					if (valueX >= valueA[c][C][add1])
					{
						valueX = valueA[c][C][add1];
						place[0] = c;
						place[1] = C;
					}

				}

			}

		}

		panel[place[0]][place[1]] = "O";
		falsePanel[place[0]][place[1]] = "O";
		for (size_t i = 0; i < 3; i++)
		{
			for (size_t I = 0; I < 3; I++)
			{
				for (size_t N = 0; N < 10; N++)
				{
					valueA[i][I][N] = NULL;
				}
			}

		}
	}
	else
	{
		for (int d = 0; d < 3; d++)
		{
			for (int D = 0; D < 3; D++)
			{
				if (falseTurn % 2 == 1)
				{
					if (valueA[d][D][add1] >= -10 && valueA[d][D][add1] <= 10)
					{
						if (valueX >= valueA[d][D][add1])
						{
							valueX = valueA[d][D][add1];
						}
					}
				}
				else
				{
					if (valueA[d][D][add1] >= -10 && valueA[d][D][add1] <= 10)
					{
						if (valueO <= valueA[d][D][add1])
						{
							valueO = valueA[d][D][add1];
						}
					}
				}
			}

		}
		if (falseTurn % 2 == 1)
		{
			return valueX;

		}
		else
		{
			return valueO;

		}

	}
	return 0;
}
int Board::ComWinCheck()
{
	for (int i = 0; i < 3; i++) {
		if (panel[i][0] == "O" && panel[i][1] == "O" && panel[i][2] == "_") {
			panel[i][2] = "O";
			return 0;
		}
		if (panel[i][0] == "O" && panel[i][1] == "_" && panel[i][2] == "O") {
			panel[i][1] = "O";
			return 0;
		}
		if (panel[i][0] == "_" && panel[i][1] == "O" && panel[i][2] == "O") {
			panel[i][0] = "O";
			return 0;
		}
		if (panel[0][i] == "O" && panel[1][i] == "O" && panel[2][i] == "_") {
			panel[2][i] = "O";
			return 0;
		}
		if (panel[0][i] == "O" && panel[1][i] == "_" && panel[2][i] == "O") {
			panel[1][i] = "O";
			return 0;
		}
		if (panel[0][i] == "_" && panel[1][i] == "O" && panel[2][i] == "O") {
			panel[0][i] = "O";
			return 0;
		}
	}
	if (panel[0][0] == "O" && panel[1][1] == "O" && panel[2][2] == "_") {
		panel[2][2] = "O";
		return 0;
	}
	if (panel[0][0] == "O" && panel[1][1] == "_" && panel[2][2] == "O") {
		panel[1][1] = "O";
		return 0;
	}
	if (panel[0][0] == "_" && panel[1][1] == "O" && panel[2][2] == "O") {
		panel[0][0] = "O";
		return 0;
	}
	if (panel[0][2] == "O" && panel[1][1] == "O" && panel[2][0] == "_") {
		panel[2][0] = "O";
		return 0;
	}
	if (panel[0][2] == "O" && panel[1][1] == "_" && panel[2][0] == "O") {
		panel[1][1] = "O";
		return 0;
	}
	if (panel[0][2] == "_" && panel[1][1] == "O" && panel[2][0] == "O") {
		panel[0][2] = "O";
		return 0;
	}
	else {
		return 1;
	}
}
void Board::PlayerTurn(string player)
{
	int across;
	int down;
	string playerX = "X";
	cout << player << " turn" << endl;
	cout << "input the column from 0-2 starting from the left" << endl;
	cin >> across;
	cout << "input the row from 0-2 starting from the top" << endl;
	cin >> down;
	if (panel[across][down] == "_")
	{
		setValues(across, down, player, panel);
		setValues(across, down, player, falsePanel);
	}
	else
	{
		system("cls");
		Show(panel);
		cout << " That space taken already!!! " << endl;
		PlayerTurn(player);
	}
}
int main()
{

	string board[3][3] = { "_","_","_","_","_","_","_","_","_" };
	string playerX = "X";
	string playerO = "O";
	string player;
	Turn turncount;
	Board Game;

	while (Game.Win(player, Game.panel) == 2)
	{

		if (Game.turn % 2 == 1)
		{
			player = playerO;

		}
		else
		{
			player = playerX;
		}
		Game.turn++;
		system("cls");
		Game.Show(Game.panel);
		if (player == "X")
		{
			Game.PlayerTurn(player);
		}
		else
		{
			cout << "before" << endl;
			Game.ComWinCheck();
			Game.RecursiveWinCheck();
			cout << "after" << endl;

		}

	}
	system("cls");
	Game.Show(Game.panel);
	Game.Win(player, Game.panel);
	return 0;
}
